-- This file sets features for nvim that isn't in file prefs.lua

local cmd = vim.cmd

cmd("set whichwrap+=<,>,[,]")

-- vim.cmd([[ autocmd TextChanged,FocusLost,BufEnter * silent update ]])
