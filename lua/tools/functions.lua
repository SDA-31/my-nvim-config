local this = {}

-- Separator function from plenary.path
this.sep = function()
	if jit then
		local os = string.lower(jit.os)
		if os == "linux" or os == "osx" or os == "bsd" then
			return "/"
		else
			return "\\"
		end
	else
		return package.config:sub(1, 1)
	end
end

this.contains_key = function(key, table)
	return table[key] ~= nil
end

-- Table value match finder
this.contains = function(val, table)
	for _, i in pairs(table) do
		if i == val then
			return true
		end
	end
	return false
end

return this
