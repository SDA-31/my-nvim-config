-- Telescope settings

local this = {}

this.setup = function(prefix)
	local telescope = require("telescope")

	local opts = require(prefix .. ".telescope_handlers").opts

	local telescope_plugins = require(
		"plug_settings.telescope.telescope_plugins" --[[ prefix .. "telescope_plugins" ]]
	):new(prefix)

	opts = vim.tbl_deep_extend("force", opts, telescope_plugins:get_plugins_opts())

	telescope.setup(opts)

	telescope_plugins:setup_plugins()

	-- To get fzf loaded and working with telescope, you need to call
	-- load_extension, somewhere after setup function:
	telescope.load_extension("fzf")
end

return this
