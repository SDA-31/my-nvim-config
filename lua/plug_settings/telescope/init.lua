local telescope = require("tools.reqall")("telescope")
if not telescope then
	return
end

local this = {}

this.setup = function(prefix)
	local new_prefix = prefix .. ".telescope"

	require(new_prefix .. ".telescope_conf").setup(new_prefix)
end

return this
