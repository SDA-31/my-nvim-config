local this = {}

function this:new(prefix)
	local obj = {}

	local telescope = require("telescope")

	local telescope_prefix = prefix

	local handlers = require(telescope_prefix .. ".telescope_handlers")

	function this:get_plugins_opts()
		local exts = {}

		for _, i in ipairs(handlers.plugins_list) do
			local is_valid, plug_opts = pcall(require, telescope_prefix .. ".plug_settings." .. i)
			if is_valid then
				exts = vim.tbl_deep_extend("force", exts, plug_opts)
			end
		end

		return {
			extensions = exts,
		}
	end

	function this:setup_plugins()
		for _, i in ipairs(handlers.plugins_list) do
			telescope.load_extension(i)
		end
	end

	setmetatable(obj, self)
	self.__index = self
	return obj
end

return this
