-- CMP plugins

local cmp = require "cmp"
cmp.setup {
	sources = {
		{ name = "buffer" },
		{ name = "cmp-path" },
		{ name = "cmp-tabnine" },
		{ name = "luasnip", option = { use_show_condition = false } },
		{ name = "nvim_lsp" },
		{ name = "nvim_lsp_document_symbol" },
		{ name = "nvim_lsp_signature_help" },
	}
}

cmp.setup.cmdline("/", {
	sources = cmp.config.sources({
		{ name = "nvim_lsp_document_symbol" }
	},
		{
			{ name = "buffer" }
		})
})
