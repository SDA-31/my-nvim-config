-- This file contains nerdtree git plugin settings

-- Custom icons
vim.g.NERDTreeGitStatusIndicatorMapCustom = {
	Modified =	"✹",
	Staged =	"✚",
	Untracked =	"✭",
	Renamed =	"➜",
	Unmerged =	"═",
	Deleted =	"✖",
	Dirty =		"✗",
	Ignored =	"☒",
	Clean =		"✔︎",
	Unknown =	"?",
}

-- NerdFonts
vim.g.NERDTreeGitStatusUseNerdFonts = 1 -- you should install nerdfonts by yourself. default: 0

-- Ignored
vim.g.NERDTreeGitStatusShowIgnored = 1 -- a heavy feature may cost much more time. default: 0

-- Untracked
vim.g.NERDTreeGitStatusUntrackedFilesMode = "all" -- a heavy feature too. default: normal

-- Clean
vim.g.NERDTreeGitStatusShowClean = 1 -- default: 0

-- Brackets
vim.g.NERDTreeGitStatusConcealBrackets = 1 -- default: 0

vim.cmd [[ let NERDTreeShowHidden=1 ]]
-- Binpath
-- vim.g.NERDTreeGitStatusGitBinPath = '/your/file/path' -- default: git (auto find in path)

