local req = require("tools.require_table")

local prefix = "plug_settings"

local requirements = {
	"prefs",
	"autosave_conf",
	"bufferline_conf",
	"comment_conf",
	"nerdtree_git_conf",
	"vim_airline_conf",
	"git_signs_conf",
	"neogit_conf",
	"lsp",
	"cmp",
	"telescope",
}

req(prefix, requirements, { "lsp", "cmp", "telescope" }, prefix)
