-- This file sets the behavior of the plugin manager, as well as the plugins that need to be installed
local fn = vim.fn
-- Automatically install packer
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
local os = string.lower(jit.os)
if not (os == "linux" or os == "osx" or os == "bsd") then
	install_path = install_path:gsub("/", "\\")
end

if fn.empty(fn.glob(install_path)) > 0 then
	PACKER_BOOTSTRAP = fn.system({
		"git",
		"clone",
		"--depth",
		"1",
		"https://github.com/wbthomason/packer.nvim",
		install_path,
	})
	print("Installing packer close and reopen Neovim...")
	vim.cmd([[packadd packer.nvim]])
end

-- Autocommand that reloads neovim whenever you leave plugins.lua file buffer
vim.cmd([[
	augroup packer_user_config
		autocmd!
		autocmd BufUnload plugins.lua source <afile> | PackerSync
	augroup end
]])

-- Use a protected call so we don't error out on first use
local is_valid, packer = pcall(require, "packer")
if not is_valid then
	return false
end

-- Have packer use a popup window
packer.init({
	display = {
		open_fn = function()
			return require("packer.util").float({ border = "rounded" })
		end,
	},
})

-- !!!Install your plugins here
return packer.startup(function(use)
	-- My plugins here
	-- API
	use("nvim-lua/plenary.nvim") -- Useful lua functions used ny lots of plugins
	use("nvim-lua/popup.nvim") -- An implementation of the Popup API from vim in Neovim
	use("wbthomason/packer.nvim") -- Have packer manage itself

	-- Useful plugins
	use("arthurxavierx/vim-caser") -- Case switcher
	use("b0o/SchemaStore.nvim") -- Json schemas for jsonls
	use("chrisbra/csv.vim") -- CSV editor
	use("fedepujol/move.nvim") -- Move lines
	use("kazhala/close-buffers.nvim") -- Closing multiple buffers
	use("mg979/vim-visual-multi") -- Multiple cursors
	use("numToStr/Comment.nvim") -- Commentary plugin
	use("preservim/nerdtree") -- Useful file explorer
	use("preservim/tagbar") -- Show tags for current file
	use("tpope/vim-surround") -- Surrounding strings
	use("vim-airline/vim-airline") -- Beautiful status line
	use({
		"akinsho/bufferline.nvim", -- Buffer line
		tag = "*",
		requires = "kyazdani42/nvim-web-devicons",
	})
	use({
		"iamcco/markdown-preview.nvim", -- Markdown preview in browser
		ft = "markdown",
		run = "cd app && yarn install",
	})
	use({
		"folke/todo-comments.nvim", -- Todo comments finder
		requires = "nvim-lua/plenary.nvim",
	})
	use("907th/vim-auto-save") -- Autosave plugin

	-- Visual
	use("ap/vim-css-color") -- CSS colors preview
	use("ryanoasis/vim-devicons") -- Icons for NerdTree

	-- Colorschemes
	use("bluz71/vim-nightfly-guicolors")
	use("dracula/vim")
	use("ghifarit53/tokyonight-vim")
	use("joshdick/onedark.vim")
	use("rakr/vim-one")
	use("xiyaowong/nvim-transparent") -- Transparent colors
	use({
		"vim-airline/vim-airline-themes", -- Airline themes
		requires = "vim-airline/vim-airline",
	})

	-- Git
	use("lewis6991/gitsigns.nvim") -- Git plugin for signs displaying
	use("tpope/vim-fugitive") -- Git plugin
	use({
		"rbong/vim-flog", -- Git branch visualizer
		requires = "tpope/vim-fugitive",
	})
	use({
		"TimUntersberger/neogit", -- Magit clone for NVim
		requires = { "nvim-lua/plenary.nvim", "sindrets/diffview.nvim" },
	})
	use({
		"sindrets/diffview.nvim", -- Git diff view
		requires = { "nvim-lua/plenary.nvim", "kyazdani42/nvim-web-devicons" },
	})
	use({
		"Xuyuanp/nerdtree-git-plugin", -- NerdTree visual plugin for git
		requires = { "preservim/nerdtree", "ryanoasis/vim-devicons" },
	})

	-- Telescope
	use({
		"nvim-telescope/telescope.nvim", -- Telescope finder
		requires = { "nvim-lua/plenary.nvim", "kyazdani42/nvim-web-devicons" },
	})
	use({
		"nvim-telescope/telescope-fzf-native.nvim", -- Telescope sorter
		requires = "nvim-telescope/telescope.nvim",
		run = "make",
	})
	use({
		"nvim-telescope/telescope-symbols.nvim", -- Telescope symbols menu
		requires = "nvim-telescope/telescope.nvim",
	})
	use({
		"nvim-telescope/telescope-media-files.nvim", -- Telescope media viewer
		requires = {
			"nvim-lua/popup.nvim",
			"nvim-lua/plenary.nvim",
			"nvim-telescope/telescope.nvim",
		},
	})
	use({
		"gbrlsnchs/telescope-lsp-handlers.nvim", -- Telescope LSP handlers
		requires = { "nvim-lua/popup.nvim", "nvim-lua/plenary.nvim" },
	})

	-- CMP
	use("hrsh7th/nvim-cmp") -- The completion plugin
	-- use "windwp/nvim-autopairs" -- Autopairs for quotes, braces etc.
	use({
		"onsails/lspkind.nvim", -- Pictogram formating
		requires = "hrsh7th/nvim-cmp",
	})

	-- Snippets
	use("hrsh7th/cmp-buffer") -- Buffer completions
	use("hrsh7th/cmp-cmdline") -- Cmdline completions
	use("L3MON4D3/LuaSnip") -- Snippet engine
	use("rafamadriz/friendly-snippets") -- A bunch of snippets to use
	use({
		"hrsh7th/cmp-nvim-lsp-document-symbol", -- Source for textDocument/documentSymbol
		requires = { "hrsh7th/nvim-cmp", "hrsh7th/cmp-buffer" },
	})
	use({ "hrsh7th/cmp-nvim-lsp", requires = "hrsh7th/nvim-cmp" }) -- LSP completion
	use({ "hrsh7th/cmp-path", requires = "hrsh7th/nvim-cmp" }) -- Path completion
	use({ "saadparwaiz1/cmp_luasnip", requires = "hrsh7th/nvim-cmp" }) -- Luasnip completion
	use({ "tzachar/cmp-tabnine", run = "./install.sh", requires = "hrsh7th/nvim-cmp" }) -- Tabnine

	-- LSP
	use("neovim/nvim-lspconfig") -- Collection of configurations for the built-in LSP client
	use({ "williamboman/nvim-lsp-installer", requires = "neovim/nvim-lspconfig" }) -- LSP installer

	-- Formatting support
	use("jose-elias-alvarez/null-ls.nvim")

	-- Additional languages support
	use("udalov/kotlin-vim")

	-- Automatically set up your configuration after cloning packer.nvim
	-- Put this at the end after all plugins
	if PACKER_BOOTSTRAP then
		require("packer").sync()
	end
end)
